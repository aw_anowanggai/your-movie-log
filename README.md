# Your Movie Log - Software Engineering Final Exam

M. Fadhil Qorano Wanggai

1806173582

[yourmovielog.herokuapp.com](http://yourmovielog.herokuapp.com/)

Your Movie Log (previously known as WatchSchedule) is a Spring Boot application added with JavaScript functions

Feature List =

1. Able to search movies and directly get the information about the movie.
2. Add a movie you want to watch.
3. Add a schedule containing day and time for when to watch your movie.
4. Able to see your scheduled movies list.
5. Able to change or delete your previously scheduled movie.
6. Add a review and score for the movie you have watched.
7. Able to edit or delete your review for the movie.
