package net.javaguides.springboot.tutorial.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import net.javaguides.springboot.tutorial.entity.Movie;

@Repository
public interface MovieRepository extends CrudRepository<Movie, Long> {

    List<Movie> findByTitle(String title);

}
