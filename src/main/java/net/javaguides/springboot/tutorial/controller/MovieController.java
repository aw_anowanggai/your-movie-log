package net.javaguides.springboot.tutorial.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.Date;

import net.javaguides.springboot.tutorial.entity.Movie;
import net.javaguides.springboot.tutorial.repository.MovieRepository;

@Controller
@RequestMapping("/movies/")
public class MovieController {

    private final MovieRepository movieRepository;

    @Autowired
    public MovieController(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    @GetMapping("addmovie")
    public String showMovieForm(Movie movie) {
        return "add-movie";
    }

    @GetMapping("list")
    public String showUpdateForm(Model model) {
        model.addAttribute("movies", movieRepository.findAll());
        return "index";
    }

    @PostMapping("add")
    public String addMovie(@Valid Movie movie, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-movie";
        }

        movieRepository.save(movie);
        return "redirect:list";
    }

    @GetMapping("edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        Movie movie = movieRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid movie Id:" + id));
        model.addAttribute("movie", movie);
        return "update-movie";
    }

    @PostMapping("update/{id}")
    public String updateMovie(@PathVariable("id") long id, @Valid Movie movie, BindingResult result,
                                Model model) {
        if (result.hasErrors()) {
            movie.setId(id);
            return "update-movie";
        }

        movieRepository.save(movie);
        model.addAttribute("movies", movieRepository.findAll());
        return "index";
    }

    @GetMapping("delete/{id}")
    public String deleteMovie(@PathVariable("id") long id, Model model) {
        Movie movie = movieRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid movie Id:" + id));
        movieRepository.delete(movie);
        model.addAttribute("movies", movieRepository.findAll());
        return "index";
    }
}
