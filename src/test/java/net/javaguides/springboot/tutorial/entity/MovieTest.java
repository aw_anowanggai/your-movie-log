package net.javaguides.springboot.tutorial.entity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.time.*;
import java.util.*;
import static org.assertj.core.api.Assertions.*;


public class MovieTest {

    private Movie movie;

    @BeforeEach
    void setMovie() {
        movie = new Movie("Avengers Endgame", LocalDate.of(2019, 05, 24), LocalTime.of(10, 0), "10", "Amazing");

    }

    @Test
    void testTitle() throws Exception{
        String title = "Avengers Endgame";
        movie.setTitle(title);
        assertThat(movie.getTitle().equals("title"));
    }

    @Test
    void testDate() throws Exception{
        LocalDate date = LocalDate.of(2019, 05, 24);
        movie.setDate(date);
        assertThat(movie.getDate().equals("date"));
    }

    @Test
    void testTime() throws Exception{
        LocalTime time = LocalTime.of(10, 0);
        movie.setTime(time);
        assertThat(movie.getTime().equals("time"));
    }

    @Test
    void testScore() throws Exception {
        String score = "10";
        movie.setScore(score);
        assertThat(movie.getScore().equals("score"));
    }

    @Test
    void testReview() throws Exception {
        String review = "Amazing";
        movie.setReview(review);
        assertThat(movie.getReview().equals("review"));
    }
    











}
